import { Component, Fragment } from 'react';
import './App.css';
import Mycars from './componnents/MyCars';

class App extends Component{

    state= {
        title:'MES VOITURES'
    }

    changeColor= (e)=>{

        this.setState({
            title: 'TITRE EN DURE'
        })
        
    }

    viaParam= (title)=>{
        this.setState({
            title: title
        })
    }

    viaBiand= (param)=>{
        this.setState({
            title: param
        })
    }

    inputChange=(e)=>{
        this.setState({
            title: e.target.value
        })
    }

    render(){

        return ( 
            <div className="App">
               
                <Mycars title={this.state.title}/>
                <Fragment>
                    <button className="btn btn-primary btn-space" onClick={this.changeColor}>En dure</button>
                    <button className="btn btn-success btn-space" onClick={()=>this.viaParam('TITRE VIA PARAM')}>Via Param</button>
                    <button className="btn btn-info" onClick={this.viaBiand.bind(this, 'TITRE VIA BIND')}>Via bind</button>
                </Fragment>
                <Fragment>
                    <input type='text' hidden onChange={this.inputChange} value={this.state.title} className='form-control'/>
                </Fragment>
            </div>
        );
        }
    }

    export default App;