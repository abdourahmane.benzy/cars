import React from "react";
import './Car.css'



const Car = ({nom, color,year}) =>{

    // console.log(props)
    const colorInfo = color ? (`${color}`): ('Neant')
        return(
            <div className='car'>
                {/* <span>Vehicule: {nom}</span>
                <span>{colorInfo}</span>
                <span>Age:{year}</span> */}

                <table className='table table-stripped'>
                    <thead>
                        <tr>
                            <th>Vehicule</th>
                            <th>Couleur</th>
                            <th>Age</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{nom}</td>
                            <td>{colorInfo}</td>
                            <td>{year}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        );
    
}
export default Car;