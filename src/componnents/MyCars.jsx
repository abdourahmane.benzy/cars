import React,{ Component, Fragment } from "react";
import Car from "./Car";



class Mycars extends Component{


    state={
        voitures:[
            {nom: 'Ford', color:'red', year: '2000'},
            {nom: 'Mercedez', color:'black', year: '2010'},
            {nom: 'Renault', color:'green', year: '2020'}

        ],
        year:new Date().getFullYear()
    }


    
    noCopy=(e)=>{
        alert('merci de ne pas copier!!!')
    }



    onOverMouse =(e)=>{
        if (e.target.classList.contains('styled')) {
            e.target.classList.remove('styled')
            
        } else {
            e.target.classList.add('styled')
        }
    }

    addTen =()=>{
        const voiture= [...this.state.voitures];

       const newV= voiture.map( (car)=> car.year -= 10)

       this.setState({
             newV
        })
    }



    render(props){
        const voiture= [...this.state.voitures]
        
        return(
            <> 
                <h1 onMouseOver={this.onOverMouse}>{this.props.title}</h1>

                <p onCopy={this.noCopy}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia vel ab nobis harum  </p>
                <button onClick={this.addTen} className="btn btn-default border border-primary">Add +10</button>
                {
                    voiture.map((car,index)=>{
                        return(
                            <Fragment key={index}>
                                <Car color={car.color} nom={car.nom} year={this.state.year - car.year}/>
                            </Fragment>
                        )
                    })
                }
               
            </>
        )
    }
}
export default Mycars;